#   Shooter 2024 Pol Gonzalo
---

##  Autor
<b>Pol Gonzalo Méndez</b>

---

## Risketos

###  Risketos Bàsics
- [x] Control de personatge amb base ACharacter (o amb AController).
- [x] Capacitat de disparar, ja sigui amb Trace line (Raycast) o amb objectes físics en moviment (Projectile).
- [x] Ha d’implementar una petita inteligencia artificial als enemics.
- [x] Utilitzar delegats per fer alguna acció, per exemple el disparar.
- [x] Més d’un tipus d'armes i/o bales.
- [x] Ús de les UMacros per privatització, escritura i lectura de forma correcta.
### Risketos Adicionals
- [x] Us d’Animation Blueprint i Blendtrees.

---

## Controls
* (WASD) Moure
* (Click (Arma vermella mantenir / Arma blava simplement clicar) esquerra) Disparar
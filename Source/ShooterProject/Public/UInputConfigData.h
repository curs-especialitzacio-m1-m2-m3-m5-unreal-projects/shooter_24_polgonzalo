﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "UInputConfigData.generated.h"

class UInputAction;

UCLASS()
class SHOOTERPROJECT_API UUInputConfigData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UInputAction* mInputMove;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UInputAction* mInputLook;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UInputAction* mInputJump;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UInputAction* mInputMouseClick;
};

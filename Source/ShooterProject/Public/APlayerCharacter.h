
#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "UInputConfigData.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Character.h"
#include "APlayerCharacter.generated.h"

class UInputMappingContext;
class UBPC_Weapon;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnClick);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnRifleClick);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnRifleUnClick);

UCLASS()
class SHOOTERPROJECT_API AAPlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Functions
	AAPlayerCharacter();
	
	virtual void Tick(float DeltaTime) override;

	void MoveCallback(const FInputActionValue& InputActionValue);

 	void LookCallback(const FInputActionValue& InputActionValue);

	void MouseClickCallback(const FInputActionValue& InputActionValue);

	void MouseClickGoingCallback(const FInputActionValue& InputActionValue);

	void MouseUnClickCallback(const FInputActionValue& InputActionValue);
	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UStaticMeshComponent* GetWeapon() const { return mWeaponPoint; };
	
	UCameraComponent* GetCamera() const { return mCameraComponent; };


	//	Variables
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* mWeaponPoint;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
    UInputMappingContext* mInputMapping;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UUInputConfigData* mInputActionData;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int mMoveSpeed { 2U };
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float mCoolDownBetweenShoots { 10.f };

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float mMaxHealth { 100.f };

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float mCurrentHealth { 100.f };

	UPROPERTY(BlueprintAssignable)
	FOnClick evOnClick;

	UPROPERTY(BlueprintAssignable)
	FOnClick evRifleOnClick;

	UPROPERTY(BlueprintAssignable)
	FOnClick evRifleOnUnClick;

	
	UBPC_Weapon* mpWeaponEquiped;

protected:
	
	virtual void BeginPlay() override;

private:
	
	UPROPERTY(VisibleAnywhere)
	UCameraComponent* mCameraComponent;
	
	

};

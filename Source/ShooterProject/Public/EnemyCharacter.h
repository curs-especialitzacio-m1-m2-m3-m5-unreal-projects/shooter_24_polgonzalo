
#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Character.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "EnemyCharacter.generated.h"

UCLASS()
class SHOOTERPROJECT_API AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	
	AEnemyCharacter();
	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void OnDamageCallback(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);
	
	
	UPROPERTY(EditDefaultsOnly)
	UBoxComponent* mBoxComponent;

	UPROPERTY(BlueprintReadOnly)
	float maxHealth { 50.0f };

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float currentHealth { 20.0f };

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	float mVelocity { 350.f };

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float mDamage { 5.f };

#pragma region IA

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UAIPerceptionComponent* mUAIPerceptionComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UAISenseConfig_Sight* mUAISenseConfigSight;

	UFUNCTION()
	void PerceptionUpdatedCallback(const TArray<AActor*>& Actors);

#pragma endregion 
	
protected:
	virtual void BeginPlay() override;

private:
	void SetNewRotation(const FVector& aTargetRot);

	UPROPERTY(VisibleAnywhere)
		FRotator mCurrentRotation;
	
	UPROPERTY(VisibleAnywhere)
		FVector mSpawnPosition;
	
	UPROPERTY(VisibleAnywhere)
		FVector mCurrentVelocity;

	bool mGoBackToBase;
	FVector mNewPosition;
	float mDistanceSQRT;

};

#pragma once

#include "CoreMinimal.h"
#include "Utils.h"
#include "Components/ActorComponent.h"
#include "BPC_Weapon.generated.h"

class AAPlayerCharacter;


UENUM(BlueprintType)
enum class EWeaponType
{
	Gun UMETA(DisplayName="Gun"),
	Rifle UMETA(DisplayName="Rifle")
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTERPROJECT_API UBPC_Weapon : public UActorComponent
{
	GENERATED_BODY()

public:
	//	Functions
	UBPC_Weapon();
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	UFUNCTION(BlueprintCallable)
	void FinalizeMyCoroutine();
	
	UFUNCTION(BlueprintCallable)
		void AttachWeapon(AAPlayerCharacter* apPlayerCharacter);

	UFUNCTION(BlueprintCallable)
		void OnFireCallback();

	UFUNCTION(BlueprintCallable)
	void StartMyCoroutine();


	//	Variables
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UBillboardComponent* mpMuzzleOffset;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float mShootRange { 5000.f };

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float mDamage { 2.f };

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	AAPlayerCharacter* mOwnerCharacter;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		EWeaponType WeaponType {EWeaponType::Gun};

private:
	FTimerHandle Handler;
		
};


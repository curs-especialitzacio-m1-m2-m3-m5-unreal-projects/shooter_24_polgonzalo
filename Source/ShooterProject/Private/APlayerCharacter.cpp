

#include "APlayerCharacter.h"
#include "EnhancedInputSubsystems.h"
#include "Utils.h"
#include "UInputConfigData.h"
#include "EnhancedInputComponent.h"
#include "Components/CapsuleComponent.h"
#include "ShooterProject/Public/Utils.h"


AAPlayerCharacter::AAPlayerCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	
	mWeaponPoint = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponPoint"));
	mWeaponPoint -> bCastDynamicShadow = false;
	mWeaponPoint -> CastShadow = false;

	mCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraPlayer"));
	mCameraComponent->SetupAttachment(GetCapsuleComponent());
	mCameraComponent-> bUsePawnControlRotation = true;
	
	
}

void AAPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	mWeaponPoint->AttachToComponent(GetMesh(), {EAttachmentRule::SnapToTarget, true}, TEXT("WeaponPointSocket"));
	
}

void AAPlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAPlayerCharacter::MoveCallback(const FInputActionValue& InputActionValue)
{
	if(IsValid(Controller))
	{
		const FVector2D MoveValue { InputActionValue.Get<FVector2D>() };
		const FRotator MoveRotator { 0,Controller->GetControlRotation().Yaw,0 };

		if(MoveValue.Y != 0.f)
		{
			const FVector Dir {MoveRotator.RotateVector(FVector::ForwardVector)};
			AddMovementInput(Dir, MoveValue.Y);
		}
		if(MoveValue.X != 0.f)
		{
			const FVector Dir {MoveRotator.RotateVector(FVector::RightVector)};
			AddMovementInput(Dir, MoveValue.X);
		}
		
	}
}

void AAPlayerCharacter::LookCallback(const FInputActionValue& InputActionValue)
{
	if(IsValid(Controller))
	{
		const FVector2D valueLook { InputActionValue.Get<FVector2D>() };

		if(valueLook.X!=0.f)
		{
			AddControllerYawInput(valueLook.X);
		}

		if(valueLook.Y!=0.f)
		{
			
			AddControllerPitchInput(valueLook.Y);
		}
	}
}

void AAPlayerCharacter::MouseClickCallback(const FInputActionValue& InputActionValue)
{
	if(IsValid(Controller))
	{
		evOnClick.Broadcast();
	}
	
}

void AAPlayerCharacter::MouseClickGoingCallback(const FInputActionValue& InputActionValue)
{
	if(IsValid(Controller))
	{
		evRifleOnClick.Broadcast();
	}
}

void AAPlayerCharacter::MouseUnClickCallback(const FInputActionValue& InputActionValue)
{
	if(IsValid(Controller))
	{
		evRifleOnUnClick.Broadcast();
	}
}

void AAPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);
	
	APlayerController* mPlayerController = Cast<APlayerController>(GetController());
	UEnhancedInputLocalPlayerSubsystem* EInputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(mPlayerController->GetLocalPlayer());
	EInputSubsystem->ClearAllMappings();
	EInputSubsystem->AddMappingContext(mInputMapping, 0);

	UEnhancedInputComponent* EInputComponent {Cast<UEnhancedInputComponent>(PlayerInputComponent)};
	
	EInputComponent->BindAction(mInputActionData->mInputMove, ETriggerEvent::Triggered, this, &AAPlayerCharacter::MoveCallback);
	EInputComponent->BindAction(mInputActionData->mInputJump, ETriggerEvent::Triggered, this, &AAPlayerCharacter::Jump);
	EInputComponent->BindAction(mInputActionData->mInputLook, ETriggerEvent::Triggered, this, &AAPlayerCharacter::LookCallback);
	EInputComponent->BindAction(mInputActionData->mInputMouseClick, ETriggerEvent::Started, this, &AAPlayerCharacter::MouseClickCallback);
	EInputComponent->BindAction(mInputActionData->mInputMouseClick, ETriggerEvent::Started, this, &AAPlayerCharacter::MouseClickGoingCallback);
	EInputComponent->BindAction(mInputActionData->mInputMouseClick, ETriggerEvent::Completed, this, &AAPlayerCharacter::MouseUnClickCallback);
		
}


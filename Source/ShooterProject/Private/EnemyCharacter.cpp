

#include "EnemyCharacter.h"

#include "APlayerCharacter.h"
#include "Components/CapsuleComponent.h"
#include "ShooterProject/Public/Utils.h"

AEnemyCharacter::AEnemyCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	GetCapsuleComponent()->RemoveFromRoot();
	
	mBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Body"));
	mBoxComponent->SetupAttachment(RootComponent);

	mUAISenseConfigSight = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("AISightConfig"));
	mUAISenseConfigSight -> SightRadius = 1250.f;
	mUAISenseConfigSight -> LoseSightRadius = 1300.f;
	mUAISenseConfigSight -> PeripheralVisionAngleDegrees = 90.f;
	mUAISenseConfigSight -> DetectionByAffiliation.bDetectFriendlies =
		mUAISenseConfigSight -> DetectionByAffiliation.bDetectEnemies =
			mUAISenseConfigSight -> DetectionByAffiliation.bDetectNeutrals = true;
	mUAISenseConfigSight->SetMaxAge(.1f);

	mUAIPerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerceptionComponent"));
	mUAIPerceptionComponent->ConfigureSense(*mUAISenseConfigSight);
	mUAIPerceptionComponent->SetDominantSense(mUAISenseConfigSight->GetSenseImplementation());
	mUAIPerceptionComponent->OnPerceptionUpdated.AddDynamic(this, &AEnemyCharacter::PerceptionUpdatedCallback);
	
	OnTakeAnyDamage.AddDynamic(this, &AEnemyCharacter::OnDamageCallback);

	mCurrentVelocity = FVector::Zero();
	mDistanceSQRT = BIG_NUMBER;
	

}

void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();

	if(currentHealth != maxHealth)
	{
		currentHealth = maxHealth;
	}
}

void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(!mCurrentVelocity.IsZero())
	{
		mNewPosition = GetActorLocation() + mCurrentVelocity * DeltaTime;

		if(mGoBackToBase)
		{
			double SQRT = (mNewPosition - mSpawnPosition).SizeSquared2D();
			if(SQRT < mDistanceSQRT)
			{
				mDistanceSQRT = SQRT;
			}
			else
			{
				mDistanceSQRT = BIG_NUMBER;
				mGoBackToBase = false;
				SetNewRotation(GetActorForwardVector());
				mCurrentVelocity = FVector::ZeroVector;
			}
		}
		SetActorLocation(mNewPosition);
	}


}

void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemyCharacter::OnDamageCallback(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser)
{
	if(currentHealth-Damage<=0)
	{
		Destroy();
	}
	currentHealth-=Damage;
	//ScreenD2("Health: %f ",  currentHealth)
}

void AEnemyCharacter::PerceptionUpdatedCallback(const TArray<AActor*>& Actors)
{
	for(const auto& UpdatedActor : Actors)
	{
		FActorPerceptionBlueprintInfo information;
		mUAIPerceptionComponent->GetActorsPerception(UpdatedActor, information);

		if(information.LastSensedStimuli[0].WasSuccessfullySensed() && Cast<AAPlayerCharacter>(UpdatedActor))
		{
			SetNewRotation(UpdatedActor->GetActorLocation());
		} else
		{
			FVector AuxDir { mSpawnPosition - GetActorLocation() };
			AuxDir.Z = 0;
			if(AuxDir.SizeSquared2D() > 1.0f)
			{
				mGoBackToBase = true;
				SetNewRotation(mSpawnPosition);
			}
		}
	}
}

void AEnemyCharacter::SetNewRotation(const FVector& aTargetRot)
{
	FVector NewDir = aTargetRot - GetActorLocation();
	NewDir.Z = 0;
	mCurrentVelocity = NewDir.GetSafeNormal() * mVelocity;
	mCurrentRotation = NewDir.Rotation();
	SetActorRotation(mCurrentRotation);

}

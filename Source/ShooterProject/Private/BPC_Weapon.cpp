#include "BPC_Weapon.h"

#include "APlayerCharacter.h"
#include "Components/BillboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "ShooterProject/Public/Utils.h"

UBPC_Weapon::UBPC_Weapon()
{
	PrimaryComponentTick.bCanEverTick = true;

	mpMuzzleOffset = CreateDefaultSubobject<UBillboardComponent>(TEXT("ShootPoint"));
}

void UBPC_Weapon::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void UBPC_Weapon::FinalizeMyCoroutine()
{
	ScreenD1("Sale")
	GetWorld()->GetTimerManager().ClearTimer(Handler);
}

void UBPC_Weapon::OnFireCallback()
{
	if( mOwnerCharacter == nullptr ) return; // If player not exist

	const UWorld* mWorld { GetWorld() };

	if ( mWorld == nullptr ) return; // If the world not exist we can't have the reference

	UCameraComponent* mCamera { mOwnerCharacter->GetCamera() };
	
	const FRotator CameraRotation { mCamera->GetComponentRotation() };

	const FVector StartLocation { GetOwner()->GetActorLocation() + CameraRotation.RotateVector( mpMuzzleOffset->GetComponentLocation() ) };
	const FVector FinalVector { StartLocation + UKismetMathLibrary::GetForwardVector(mCamera->GetComponentRotation()) * mShootRange  };

	FCollisionQueryParams QueryParams {};
	QueryParams.AddIgnoredActor(mOwnerCharacter);

	FHitResult HitResult {};
	mWorld->LineTraceSingleByChannel(HitResult, StartLocation, FinalVector, ECC_Camera, QueryParams );
	DrawDebugLine(mWorld, StartLocation, FinalVector,  HitResult.bBlockingHit ? FColor::Blue : FColor::Red, false, 5.0f, 0, 10.f);

	if(HitResult.bBlockingHit && IsValid(HitResult.GetActor()))
	{
		UGameplayStatics::ApplyDamage(HitResult.GetActor(), mDamage, mOwnerCharacter->GetController(), GetOwner(), {});
	}
	
}

void UBPC_Weapon::StartMyCoroutine()
{
	ScreenD1("Entra")
	GetWorld()->GetTimerManager().SetTimer(Handler, this, &UBPC_Weapon::OnFireCallback, 0.2f, true);
}

void UBPC_Weapon::AttachWeapon(AAPlayerCharacter* apPlayerCharacter)
{
	ScreenD1("Attach");
	mOwnerCharacter = apPlayerCharacter;
	if(mOwnerCharacter != nullptr && mOwnerCharacter->mpWeaponEquiped == nullptr)
	{
		mOwnerCharacter->mpWeaponEquiped = this;
		FAttachmentTransformRules AttachRules {EAttachmentRule::SnapToTarget, true};
		GetOwner()->AttachToComponent(mOwnerCharacter->GetWeapon(), AttachRules, FName("WeaponPointSocket"));
		if(WeaponType==EWeaponType::Gun)
		{
			mOwnerCharacter->evOnClick.AddDynamic(this, &UBPC_Weapon::OnFireCallback);
		}
		else
		{
			mOwnerCharacter->evRifleOnClick.AddDynamic(this, &UBPC_Weapon::StartMyCoroutine);
			mOwnerCharacter->evRifleOnUnClick.AddDynamic(this, &UBPC_Weapon::FinalizeMyCoroutine);
		}
	}
}


